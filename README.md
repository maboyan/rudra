# rudra #

mp4の特定のタグ(atom)を削除したり、書き換えたりするためのツール。
goの勉強をしたかったためgoで作成しました。

### usage ###

rudra.exeと同じ階層にsetting.jsonを置いた状態で、

```
#!cmd
rudra.exe <path> <path> ...
```

をという感じに実行します。

### setting.json ###

```
#!json
{
  "RemoveAtoms": [
    {
      "Name": "----",
      "Bytes": [ 45, 45, 45, 45 ]
    },
    {
      "Name": "ownr",
      "Bytes": [ 111, 119, 110, 114 ]
    }
  ],
  "Encoder": "new encoder",
  "Backup": true
}
```

RemoveAtomsに削除したいタグ(atom)を配列で記述します。  
Nameは文字列なので目視でわかりやすいというメリットがある反面、文字コード的に表現できない場合があるため、  
そういった場合のためにBytesが存在します。  

Encoderを設定することで既存のEncoder用タグ(atom)を変更できます。  

Backupがtrueの場合は実行ディレクトリにbakディレクトリが作成され、  
そこにファイル構造変更前のファイルが保存されます。  