package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"./mp4"
)

// Setting 設定ファイルの構造体
type Setting struct {
	RemoveAtoms []mp4.AtomName
	Encoder     string
	Backup      bool
	Purchaser	bool
}

// SerializeSetting 設定ファイルを書き込みます
func SerializeSetting(path string, setting *Setting) error {
	if setting == nil {
		return nil
	}

	json, err := json.MarshalIndent(setting, "", "  ")
	if err != nil {
		return err
	}

	writeErr := ioutil.WriteFile(path, json, os.ModePerm)
	if writeErr != nil {
		return writeErr
	}

	return nil
}

// DeserializeSetting 設定ファイルを読み込みます
func DeserializeSetting(path string) (*Setting, error) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	result := &Setting{}
	err = json.Unmarshal(file, result)
	if err != nil {
		return nil, err
	}

	return result, nil
}
