package mp4

import (
	"encoding/binary"
	"fmt"
)

// RemovePurchaser 購入者情報を削除します
func RemovePurchaser(root IAtom) error {
	err := removePurchaser(root)
	return err
}

func removePurchaser(atom IAtom) error {
	appleAtom := NewAtomNameString("stsd")

	childCount := atom.GetChildCount()
	for i := 0; i < childCount; i++ {
		child := atom.GetChild(i)
		childName := child.GetName()

		if appleAtom.ToString() == childName.ToString() {
			// 購入者情報が入っているタグを見つけたので
			// 名前の部分だけ削除
			err := removePurchaserName(child)
			if err != nil {
				return err
			}

			// 名前部分の削除ができたので終了
			break
		} else {
			err := removePurchaser(child)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func removePurchaserName(atom IAtom) error {
	// nameと書かれた部分を探す
	content := atom.GetContent()
	contentSize := atom.GetContentSize()
	searchSize := contentSize - 4
	for i := uint64(0); i < searchSize; i++ {
		tmpName := string(content[i : i+4])

		if tmpName == "name" {
			// 発見
			tmpSize := content[i-4 : i]
			size := uint64(binary.BigEndian.Uint32(tmpSize))

			// largeサイズは未対応
			if size <= 8 {
				err := fmt.Errorf("large size atom not implemented")
				return err
			}

			// サイズがおかしい・・・
			if i+size > contentSize {
				err := fmt.Errorf("name size is invalid")
				return err
			}

			// name部分を潰します
			fillSize := size - 8
			j := uint64(0)
			for j = 0; j < fillSize; j++ {
				content[i+4+j] = 0x00
			}

			// 名前部分の削除ができたので終了
			break
		}
	}

	return nil
}
