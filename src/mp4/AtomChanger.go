package mp4

import (
	"fmt"
	"log"

	"math"
)

// ChangeEncoder エンコーダーatomの名前を第二引数で指定した文字列に変換します
func ChangeEncoder(root IAtom, encoder string) error {
	remainSize := changeEncoder(root, encoder)
	if remainSize > 0 {
		err := fmt.Errorf("エンコーダの変更に失敗しました")
		return err
	}

	return nil
}

func changeEncoder(atom IAtom, encoder string) int64 {
	encoderAtom := NewAtomNameBytes([]byte{0xa9, 0x74, 0x6f, 0x6f})
	freeAtom := NewAtomNameString("free")

	childCount := atom.GetChildCount()
	diffSize := int64(0)
	for i := 0; i < childCount; i++ {
		child := atom.GetChild(i)
		childName := child.GetName()

		if encoderAtom.ToString() == childName.ToString() {
			// エンコーダータグが見つかったら書き換え
			// 書き換えたらfreeでなんとかするサイズを計算
			beforeSize := int64(child.GetSize())
			setEncoder(child, encoder)
			afterSize := int64(child.GetSize())
			diffSize = diffSize + afterSize - beforeSize
		} else {
			// 見つからないので孫まで探す
			diffSize = diffSize + changeEncoder(child, encoder)
		}
	}

	// 特に書き換えが起きなかったので終了
	if diffSize == 0 {
		return 0
	}

	atomName := atom.GetName()
	if freeAtom.ToString() == atomName.ToString() {
		// 自分自身がfree
		newDiffSize, err := changeFreeSize(atom, diffSize)
		if err != nil {
			log.Println(err.Error())
			return newDiffSize
		}
		diffSize = newDiffSize
	}

	// freeを削除したタグより上に追加すると
	// 動画indexの計算がずれる可能性があるので
	// 下から探していく
	for i := 0; i < childCount; i++ {
		child := atom.GetChild(i)
		childName := child.GetName()

		if freeAtom.ToString() == childName.ToString() {
			newDiffSize, err := changeFreeSize(child, diffSize)
			if err != nil {
				log.Println(err.Error())
				return newDiffSize
			}
			diffSize = newDiffSize

			// 余っているFREE部分を調整してもうまくいかないかもしれない
			if diffSize == 0 {
				break
			}
		}
	}

	return diffSize
}

func setEncoder(atom IAtom, encoder string) {
	// encoder atomはcontent部分の構造がちょっと特殊
	// content部分にもサイズが格納されている
	//   サイズ(4バイト)
	// + "data"という文字列(4バイト)
	// + flag + version(4バイト)
	// + padding(4バイト)
	// + 文字列(nバイト)
	//
	// サイズには(n+16)が値として入る

	encoderBytes := []byte(encoder)

	size := len(encoderBytes) + 16
	sizeBytes := []byte{
		byte(size >> 24 & 0xff),
		byte(size >> 16 & 0xff),
		byte(size >> 8 & 0xff),
		byte(size & 0xff),
	}

	dataBytes := []byte("data")
	flagVersion := []byte{0x0, 0x0, 0x0, 0x1}
	padding := []byte{0x0, 0x0, 0x0, 0x0}

	contentBytes := make([]byte, 0)
	contentBytes = append(contentBytes, sizeBytes...)
	contentBytes = append(contentBytes, dataBytes...)
	contentBytes = append(contentBytes, flagVersion...)
	contentBytes = append(contentBytes, padding...)
	contentBytes = append(contentBytes, encoderBytes...)

	atom.SetContent(contentBytes)
}

func changeFreeSize(atom IAtom, subSize int64) (int64, error) {
	newSize := int64(atom.GetContentSize()) - subSize

	if newSize > math.MaxInt32 {
		err := fmt.Errorf("調整後のfree sizeが大きすぎます")
		return 0, err
	}

	// このfree＋違うfreeに分割すれば解決するかもしれないけど
	// 計算が面倒くさいのでやめ
	if newSize < 0 {
		return subSize, nil
	}

	content := make([]byte, newSize)
	atom.SetContent(content)

	return 0, nil
}
