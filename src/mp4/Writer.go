package mp4

import (
	"bufio"
	"fmt"
	"os"
)

// Wirte 引数で与えられたrootをファイルに書き込みます
// root自身はプログラムの都合で存在するので書き込みません
func Wirte(root *Atom, path string) error {
	file, opneError := os.Create(path)
	if opneError != nil {
		err := fmt.Errorf("cannot create %s %s", path, opneError.Error())
		return err
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	for it := root.children.Front(); it != nil; it = it.Next() {
		t := it.Value.(IAtom)
		err := t.Export(writer)
		if err != nil {
			return err
		}
	}
	writer.Flush()

	return nil
}
