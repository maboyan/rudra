package mp4

import (
	"bufio"
	"container/list"
	"encoding/binary"
	"fmt"
	"log"
	"math"
)

// Atom IAtomを実装しているシンプルなatomクラス
type Atom struct {
	name         AtomName
	isLargeSize  bool // ８バイト版でサイズを持っているか
	contentBytes []byte
	children     *list.List
}

/******************************************************************************
 * IAtom 実装
 *****************************************************************************/

// GetName atom名を取得する
func (atom *Atom) GetName() AtomName {
	return atom.name
}

// GetSize ノード全体サイズを取得する
// サイズ格納部分 + 名前格納部分 + 本体部分 + 子要素のサイズ
// を返します
// 中で子要素のサイズを計算しているのでGetというよりCalc・・・
func (atom *Atom) GetSize() uint64 {
	// サイズ格納部分 + 名前格納部分
	headerSize := uint64(8)
	if atom.isLargeSize {
		headerSize = headerSize + 8
	}

	// 本体部分
	contentSize := atom.GetContentSize()

	// 子要素のサイズ
	childSize := uint64(0)
	if atom.children != nil {
		for it := atom.children.Front(); it != nil; it = it.Next() {
			t := it.Value.(IAtom)
			tSize := t.GetSize()
			childSize = childSize + tSize
		}
	}

	size := headerSize + contentSize + childSize
	return size
}

// GetContentSize 本体部分のみのサイズを返します
func (atom *Atom) GetContentSize() uint64 {
	size := uint64(len(atom.contentBytes))
	return size
}

// GetContent 実態のbyteを渡します
func (atom *Atom) GetContent() []byte {
	return atom.contentBytes
}

// SetContent 実態のbyteを渡します
func (atom *Atom) SetContent(bytes []byte) {
	atom.contentBytes = bytes
}

// AvailableChild 自分の要素が子供を持てるか？
// 持っていないということはContentを持っているということ
// NewAtom や Initで初期化されていないatomに対してはfalseが戻ります
func (atom *Atom) AvailableChild() bool {
	if atom == nil {
		return false
	}
	name := atom.GetNameString()
	if name == "" {
		return false
	}

	if name == "root" {
		return true
	}
	if name == "moov" {
		return true
	}
	if name == "trak" {
		return true
	}
	if name == "mdia" {
		return true
	}
	if name == "minf" {
		return true
	}
	if name == "dinf" {
		return true
	}
	if name == "stbl" {
		return true
	}
	if name == "udta" {
		return true
	}
	if name == "meta" {
		return true
	}
	if name == "ilst" {
		return true
	}

	return false
}

// AddChild 要素に子供を追加します
func (atom *Atom) AddChild(child IAtom) {
	if child == nil {
		return
	}

	// 初期化していないようなら初期化する
	if atom.children == nil {
		atom.children = list.New()
	}

	atom.children.PushBack(child)
}

// GetChildCount 子atomの数を取得する
func (atom *Atom) GetChildCount() int {
	if atom == nil {
		return 0
	}
	if atom.children == nil {
		return 0
	}

	result := atom.children.Len()
	return result
}

// GetChild i番目の要素を取得します
func (atom *Atom) GetChild(index int) IAtom {
	if atom == nil {
		return nil
	}
	if atom.children == nil {
		return nil
	}
	if atom.children.Len() <= index {
		return nil
	}

	i := 0
	for it := atom.children.Front(); it != nil; it = it.Next() {
		if i == index {
			t := it.Value.(IAtom)
			return t
		}

		i = i + 1
	}

	return nil
}

// RemoveChild 引数で指定された子要素を削除する
// 孫要素も消します
func (atom *Atom) RemoveChild(name [NameBytesSize]byte) uint64 {
	if atom.children == nil {
		return 0
	}

	// 子要素を消すのではなく、残る要素を作るという作戦
	newChildren := list.New()
	removeSize := uint64(0)
	for it := atom.children.Front(); it != nil; it = it.Next() {
		t := it.Value.(IAtom)
		tName := t.GetName()
		if tName.ToBytesArray() == name {
			removeSize = removeSize + t.GetSize()
		} else {
			newChildren.PushBack(t)
		}
	}
	atom.children = newChildren

	// 孫要素も削除しておく
	for it := atom.children.Front(); it != nil; it = it.Next() {
		t := it.Value.(IAtom)
		grandsonSize := t.RemoveChild(name)
		removeSize = removeSize + grandsonSize
	}

	// 削除した分free要素を追加
	// 子要素がないとremoveSize > 0にならないはずなので
	// free要素を追加しても問題ないはず
	if removeSize > 0 {
		atom.addFreeChild(removeSize)
		removeSize = 0
	}

	return removeSize
}

// addFreeChild 子atomにfreeがあったらそこにsize分追加して
// なければfree atomを追加する
func (atom *Atom) addFreeChild(size uint64) {
	addSize := false
	for it := atom.children.Front(); it != nil; it = it.Next() {
		t := it.Value.(IAtom)
		tName := t.GetName()
		if tName.ToString() == "free" {
			newSize := t.GetSize() + size

			// 新しくfreeを作って中身を移植するという荒業
			newFree := CreateFree(newSize)
			t.SetContent(newFree.GetContent())

			addSize = true
			break
		}
	}

	// すでに追加したっぽいので終わり
	if addSize == true {
		return
	}

	// free atomが見つからなかったので追加
	free := CreateFree(size)
	atom.AddChild(free)
}

// Import ファイルを読み込みながらatomを作成します
// 戻り値は読み込んだサイズを返しています
func (atom *Atom) Import(reader *bufio.Reader, fileSize uint64) (uint64, error) {
	if atom == nil {
		err := fmt.Errorf("Atom.Import でレシーバがnilです")
		return 0, err
	}

	// 基本情報であるサイズと名前を取得
	readSize, atomSize, basisErr := atom.readSizeAndName(reader, fileSize)
	if basisErr != nil {
		return 0, basisErr
	}
	log.Printf("Import %s(%d)\n", atom.GetNameString(), atomSize)

	// 普通の構造体をしていないものに対して特別処理
	// インターフェイスの部分実装を使うべきな気もするけど
	// 面倒くさいのでここでやってしまう
	exceptionSize, exceptionErr := atom.importException(reader, fileSize)
	if exceptionErr != nil {
		return 0, exceptionErr
	}
	readSize = readSize + exceptionSize

	// 子供を持つatomか、持たないatomかで話が変わる
	if atom.AvailableChild() {
		// 子供がいる場合は、残りはすべて子要素
		for readSize < atomSize {
			var child IAtom = &Atom{}
			childSize, childErr := child.Import(reader, fileSize)
			if childErr != nil {
				return childSize, childErr
			}

			readSize = readSize + childSize
			atom.AddChild(child)
		}
	} else {
		// 子供がいない場合は残りはすべてデータ
		contentSize := atomSize - readSize
		atom.contentBytes = make([]byte, contentSize)
		contentReadSize, contentError := reader.Read(atom.contentBytes)
		if contentError != nil {
			return 0, contentError
		}
		if uint64(contentReadSize) != contentSize {
			contentErr := fmt.Errorf("contentの想定サイズと実際のサイズが違います")
			return 0, contentErr
		}
	}

	return atomSize, nil
}

// サイズと名前部分一括取得
// 戻り値は
// (この関数内で読み込んだ長さ, atom size, error)
func (atom *Atom) readSizeAndName(reader *bufio.Reader, fileSize uint64) (uint64, uint64, error) {
	// rootは特別でatom自体に何も入っていない
	// 処理の関係でファイルサイズをatom sizeとする
	if atom.GetNameString() == "root" {
		return 0, fileSize, nil
	}

	// すでに外で処理されている
	if atom.GetNameString() != "" && atom.GetSize() != 0 {
		return 0, 0, nil
	}

	// 読み込んだ長さ
	sizeRead := uint64(0)

	// サイズ
	atomSize, sizeErr := readSize(reader)
	if sizeErr != nil {
		return 0, 0, sizeErr
	}
	sizeRead = sizeRead + 4

	// 名前
	nameSlice, nameErr := readName(reader)
	if nameErr != nil {
		return 0, 0, nameErr
	}
	if len(nameSlice) != 4 {
		err := fmt.Errorf("Atom.Import で名前が%dバイトではありません", NameBytesSize)
		return 0, 0, err
	}
	nameBytes := [NameBytesSize]byte{nameSlice[0], nameSlice[1], nameSlice[2], nameSlice[3]}
	sizeRead = sizeRead + 4

	// atom sizeが1の場合は、正確なサイズが8バイトで入っている
	if atomSize == 1 {
		var largeSizeErr error
		atomSize, largeSizeErr = readLargeSize(reader)
		if largeSizeErr != nil {
			return 0, 0, sizeErr
		}

		atom.isLargeSize = true
		sizeRead = sizeRead + 8
	}

	// 全部ちゃんと終わったのでセット
	atom.Init(nameBytes)

	return sizeRead, atomSize, nil
}

// サイズ部分読み込み
func readSize(reader *bufio.Reader) (uint64, error) {
	size := uint32(0)
	err := binary.Read(reader, binary.BigEndian, &size)
	if err != nil {
		return 0, err
	}

	result := uint64(size)
	return result, nil
}

// サイズ部分読み込み（８バイト版）
func readLargeSize(reader *bufio.Reader) (uint64, error) {
	result := uint64(0)
	err := binary.Read(reader, binary.BigEndian, &result)
	if err != nil {
		return 0, err
	}

	return result, nil
}

// 名前部分読み込み
func readName(reader *bufio.Reader) ([]byte, error) {
	const NameBufferSize int = 4
	nameBuf := make([]byte, NameBufferSize)
	readSize, sizeError := reader.Read(nameBuf)
	if sizeError != nil {
		return nil, sizeError
	}
	if readSize != NameBufferSize {
		err := fmt.Errorf("atom名の途中でファイルの末尾に到達しました")
		return nil, err
	}

	return nameBuf, nil
}

// 例外処理
func (atom *Atom) importException(reader *bufio.Reader, fileSize uint64) (uint64, error) {
	if atom == nil {
		return 0, nil
	}

	// metaタグにはVersion1バイトとフラグ3バイトが存在する
	if atom.GetNameString() == "meta" {
		const MetaBufferSize int = 4
		atom.contentBytes = make([]byte, MetaBufferSize)
		readSize, metaError := reader.Read(atom.contentBytes)
		if metaError != nil {
			return 0, metaError
		}
		if readSize != MetaBufferSize {
			metaErr := fmt.Errorf("meta特殊部分の想定サイズと実際のサイズが違います")
			return 0, metaErr
		}

		return 4, nil
	}

	return 0, nil
}

// Export 引数の場所に書き込みます
func (atom *Atom) Export(writer *bufio.Writer) error {
	if atom == nil {
		return nil
	}

	// サイズと名前書き込み
	basisErr := atom.exportSizeAndName(writer)
	if basisErr != nil {
		return basisErr
	}

	// 普通の構造体をしていないものに対して特別処理
	// インターフェイスの部分実装を使うべきな気もするけど
	// 面倒くさいのでここでやってしまう
	exceptionError := atom.exportException(writer)
	if exceptionError != nil {
		return exceptionError
	}

	// 子供を持っているかで動作が変わります
	if atom.AvailableChild() {
		for it := atom.children.Front(); it != nil; it = it.Next() {
			t := it.Value.(IAtom)
			t.Export(writer)
		}
	} else {
		contentError := atom.exportContent(writer)
		if contentError != nil {
			return contentError
		}
	}

	return nil
}

func (atom *Atom) exportSizeAndName(writer *bufio.Writer) error {
	// サイズ書き込み
	// 8バイト格納の場合があるので場合分け
	if atom.isLargeSize {
		sizeError := atom.exportLargeSizeFlag(writer)
		if sizeError != nil {
			return sizeError
		}
	} else {
		sizeError := atom.exportSize(writer)
		if sizeError != nil {
			return sizeError
		}
	}

	// 名前部分書き込みは共通
	nameError := atom.exportName(writer)
	if nameError != nil {
		return nameError
	}

	// 8バイトサイズの場合はここで本体書き込み
	if atom.isLargeSize {
		largeSizeError := atom.exportLargeSize(writer)
		if largeSizeError != nil {
			return largeSizeError
		}
	}

	return nil
}

func (atom *Atom) exportSize(writer *bufio.Writer) error {
	if atom == nil {
		return nil
	}

	size := atom.GetSize()

	if size > math.MaxUint32 {
		err := fmt.Errorf("uint32以上のatom書き込みには対応していません")
		return err
	}

	size32 := uint32(size)
	writeError := binary.Write(writer, binary.BigEndian, size32)
	if writeError != nil {
		return writeError
	}

	return nil
}

func (atom *Atom) exportLargeSizeFlag(writer *bufio.Writer) error {
	if atom == nil {
		return nil
	}

	// 8バイト格納の場合はサイズ部分は1を書く
	size := uint32(1)
	writeError := binary.Write(writer, binary.BigEndian, size)
	if writeError != nil {
		return writeError
	}

	return nil
}

func (atom *Atom) exportLargeSize(writer *bufio.Writer) error {
	if atom == nil {
		return nil
	}

	size := atom.GetSize()
	writeError := binary.Write(writer, binary.BigEndian, size)
	if writeError != nil {
		return writeError
	}

	return nil
}

func (atom *Atom) exportName(writer *bufio.Writer) error {
	if atom == nil {
		return nil
	}

	name := atom.GetNameBytes()
	bytes := name[:]
	writeSize, writeError := writer.Write(bytes)
	if writeError != nil {
		return writeError
	}
	if writeSize != len(bytes) {
		err := fmt.Errorf("Nameをすべて書き込めませんでした %d != %d", writeSize, len(bytes))
		return err
	}

	return nil
}

func (atom *Atom) exportContent(writer *bufio.Writer) error {
	if atom == nil {
		return nil
	}

	content := atom.GetContent()
	contentSize, contentError := writer.Write(content)
	if contentError != nil {
		return contentError
	}
	if contentSize != len(content) {
		err := fmt.Errorf("Contentをすべて書き込めませんでした %d != %d", contentSize, len(content))
		return err
	}

	return nil
}

// 例外処理
func (atom *Atom) exportException(writer *bufio.Writer) error {
	if atom == nil {
		return nil
	}

	// metaタグにはVersion1バイトとフラグ3バイトが存在する
	// contentに保存されているのでそれを書き込む
	if atom.GetNameString() == "meta" {
		contentError := atom.exportContent(writer)
		if contentError != nil {
			return contentError
		}
	}

	return nil
}

// Print 内部構造を標準出力に出力します
func (atom *Atom) Print(level int) {
	if atom == nil {
		return
	}

	indent := ""
	for i := 0; i < level; i++ {
		indent = indent + "  "
	}

	name := atom.GetName()
	nameStr := name.ToString()
	log.Printf("%s%s(%d)\n", indent, nameStr, atom.GetSize())

	if atom.children != nil {
		for it := atom.children.Front(); it != nil; it = it.Next() {
			t := it.Value.(IAtom)
			t.Print(level + 1)
		}
	}
}

/******************************************************************************
 * 便利メソッド
 *****************************************************************************/

// GetNameString 文字列形式の名前を取得します
func (atom *Atom) GetNameString() string {
	if atom == nil {
		return ""
	}

	name := atom.GetName()
	result := name.ToString()

	return result
}

// GetNameBytes バイト形式の名前を取得します
func (atom *Atom) GetNameBytes() [NameBytesSize]byte {
	var result [NameBytesSize]byte
	if atom == nil {
		return result
	}

	name := atom.GetName()
	result = name.ToBytesArray()

	return result
}

/******************************************************************************
 * インスタンス初期化
 *****************************************************************************/

// Init すでにNew済みのインスタンスを初期化
func (atom *Atom) Init(nameBytes [NameBytesSize]byte) {
	if atom == nil {
		return
	}

	// 文字列にするためにスライスへ
	slice := nameBytes[:]
	str := string(slice)
	name := AtomName{Bytes: nameBytes, Name: str}

	atom.name = name
}

// NewAtom Atomのコンストラクタ的な何か
func NewAtom(nameBytes [NameBytesSize]byte) *Atom {
	// 文字列にするためにスライスへ
	slice := nameBytes[:]
	str := string(slice)

	name := NewAtomName(slice, str)

	result := new(Atom)
	result.name = *name

	return result
}

// CreateRoot Root要素という規格的には存在しないatomを返します
func CreateRoot() *Atom {
	slice := []byte("root")
	bytes := [NameBytesSize]byte{slice[0], slice[1], slice[2], slice[3]}

	result := NewAtom(bytes)

	return result
}

// CreateFree sizeで指定されたfree要素を作成します
func CreateFree(size uint64) *Atom {
	slice := []byte("free")
	bytes := [NameBytesSize]byte{slice[0], slice[1], slice[2], slice[3]}

	result := NewAtom(bytes)

	contentSize := size - 8
	content := make([]byte, contentSize)
	result.contentBytes = content

	return result
}
