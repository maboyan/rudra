package mp4

// AtomName タグ名を表す構造体
// BytesとNameだったらBytesのほうが優先度が高い
type AtomName struct {
	Name  string
	Bytes [NameBytesSize]byte
}

// ToString string形式に変換します
func (atom *AtomName) ToString() string {
	if atom == nil {
		return ""
	}

	bytes := atom.ToBytes()
	result := string(bytes)
	return result
}

// ToBytes 中身がstring形式だったとしてもByte形式で返してくれます
// Bytes と Name両方に入っていたらBytesを信じます
func (atom *AtomName) ToBytes() []byte {
	if atom == nil {
		return nil
	}

	allZero := true
	for _, v := range atom.Bytes {
		if v != 0 {
			allZero = false
			break
		}
	}

	// 0じゃない要素が入っているので、バイトでそのまま入っていた
	if allZero == false {
		return atom.Bytes[:]
	}

	// Nameをバイト列にします
	bytes := []byte(atom.Name)
	if len(bytes) != 4 {
		return nil
	}

	return bytes
}

// ToBytesArray ToBytesの配列版
func (atom *AtomName) ToBytesArray() [NameBytesSize]byte {
	var result [NameBytesSize]byte
	if atom == nil {
		return result
	}

	bytes := atom.ToBytes()
	if len(bytes) != 4 {
		return result
	}

	result[0] = bytes[0]
	result[1] = bytes[1]
	result[2] = bytes[2]
	result[3] = bytes[3]

	return result
}

// NewAtomName bytesとnameを指定したインスタンス作成
func NewAtomName(bytes []byte, name string) *AtomName {
	if len(bytes) != NameBytesSize {
		return nil
	}
	if len(name) != NameBytesSize {
		return nil
	}

	result := new(AtomName)
	result.Name = name
	result.Bytes[0] = bytes[0]
	result.Bytes[1] = bytes[1]
	result.Bytes[2] = bytes[2]
	result.Bytes[3] = bytes[3]

	return result
}

// NewAtomNameString 文字列だけを指定したインスタンス作成
func NewAtomNameString(str string) *AtomName {
	if len(str) != 4 {
		return nil
	}

	bytes := []byte(str)

	result := NewAtomName(bytes, str)
	return result
}

//NewAtomNameBytes bytesを指定したインスタンス作成
func NewAtomNameBytes(bytes []byte) *AtomName {
	if len(bytes) != NameBytesSize {
		return nil
	}

	str := string(bytes)

	result := NewAtomName(bytes, str)
	return result
}
