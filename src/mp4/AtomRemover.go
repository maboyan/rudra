package mp4

import (
	"fmt"
	"log"
)

// RemoveAtom 第一引数で与えられたatom以下から
// 第二引数のatomを削除します
func RemoveAtom(root IAtom, atoms []AtomName) error {
	removeSize := uint64(0)
	for _, atom := range atoms {
		slice := atom.ToBytes()
		if len(slice) != 4 {
			log.Printf("%sは%dバイトではないため削除を行いません", string(slice), NameBytesSize)
			continue
		}

		bytes := [NameBytesSize]byte{slice[0], slice[1], slice[2], slice[3]}
		removeSize = removeSize + root.RemoveChild(bytes)
	}

	if removeSize > 0 {
		err := fmt.Errorf("削除を行いましたが、free atomを追加できませんでした")
		return err
	}

	return nil
}
