package mp4

import "os"
import "bufio"
import "fmt"
import "math"

// Read 引数で与えられたファイルを読み込んで内部形式にします
func Read(path string) (IAtom, error) {
	file, opneError := os.Open(path)
	if opneError != nil {
		err := fmt.Errorf("cannot open %s %s", path, opneError.Error())
		return nil, err
	}
	defer file.Close()

	stats, statsError := file.Stat()
	if statsError != nil {
		err := fmt.Errorf("cannot read %s %s", path, statsError.Error())
		return nil, err
	}

	// Import開始
	size := uint64(stats.Size())
	if size > math.MaxInt32 {
		err := fmt.Errorf("too mach file size %s %d", path, size)
		return nil, err
	}
	reader := bufio.NewReaderSize(file, int(size))
	result := CreateRoot()
	result.Import(reader, size)

	return result, nil
}
