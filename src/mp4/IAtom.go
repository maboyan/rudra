package mp4

import "bufio"

// NameBytesSize atom名を示すバイト数
const NameBytesSize = 4

// IAtom atomを表す基本I/F
type IAtom interface {
	GetName() AtomName
	GetSize() uint64
	GetContentSize() uint64

	GetContent() []byte
	SetContent(bytes []byte)

	AvailableChild() bool

	AddChild(child IAtom)
	GetChildCount() int
	GetChild(index int) IAtom
	RemoveChild(name [NameBytesSize]byte) uint64

	Import(reader *bufio.Reader, fileSize uint64) (uint64, error)
	Export(writer *bufio.Writer) error

	Print(level int)
}
