package main

import (
	"io"
	"log"
	"path"

	"os"

	"path/filepath"

	"./mp4"
)

const backupDirectory = "bak"
const settingFileName = "setting.json"
const logFileName = "log.txt"

func main() {
	// ログ設定
	logfile, err := os.OpenFile(logFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		panic(err.Error())
	}
	defer logfile.Close()
	log.SetOutput(io.MultiWriter(logfile, os.Stdout))
	log.SetFlags(log.Ldate | log.Ltime)

	// 設定ファイル読み込み
	setting, settingErr := DeserializeSetting(settingFileName)
	if settingErr != nil {
		log.Println(settingErr.Error())
		os.Exit(1)
	}

	// 実行ディレクトリにbakディレクトリ作成
	if setting.Backup {
		dir := path.Dir(os.Args[0])
		os.Chdir(dir)
		_, bakErr := os.Stat(backupDirectory)
		if bakErr != nil {
			log.Println("[info] create backup directory")
			os.MkdirAll(backupDirectory, 777)
		}
	}

	// ファイル単位で処理
	for i, arg := range os.Args {
		// i == 0は実行パスなので弾く
		if i == 0 {
			continue
		}

		rudra(arg, setting)
	}
}

func rudra(path string, setting *Setting) {
	_, fileError := os.Stat(path)
	if fileError != nil {
		log.Printf("%sが存在しません", path)
		return
	}

	log.Println("/////////////////////////////////////")
	log.Printf("  %s\n", path)

	// バックアップに移動
	if setting.Backup {
		name := filepath.Base(path)
		bakAfter := backupDirectory + "\\" + name

		log.Printf("[info] backup %s => %s\n", path, bakAfter)
		bakErr := copyFile(path, bakAfter)
		if bakErr != nil {
			log.Println(bakErr.Error())
			return
		}
	}

	log.Println("[info] read " + path)
	root, err := mp4.Read(path)
	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Println("[info] before")
	root.Print(0)

	log.Println("[info] remove atom")
	removeErr := mp4.RemoveAtom(root, setting.RemoveAtoms)
	if removeErr != nil {
		log.Println(removeErr.Error())
		return
	}

	if setting.Encoder != "" {
		log.Println("[info] change encoder")
		encoderErr := mp4.ChangeEncoder(root, setting.Encoder)
		if encoderErr != nil {
			log.Println(encoderErr.Error())
			return
		}
	}

	if setting.Purchaser {
		log.Println("[info] remove purchaser info")
		purchacerErr := mp4.RemovePurchaser(root)
		if purchacerErr != nil {
			log.Println(purchacerErr.Error())
			return
		}
	}

	log.Println("[info] after")
	root.Print(0)

	log.Println("[info] write " + path)
	writeRoot := root.(*mp4.Atom)
	writeError := mp4.Wirte(writeRoot, path)
	if writeError != nil {
		log.Println(writeError.Error())
		return
	}
}

func copyFile(srcName string, dstName string) error {
	src, err := os.Open(srcName)
	if err != nil {
		return err
	}
	defer src.Close()

	dst, err := os.Create(dstName)
	if err != nil {
		return err
	}
	defer dst.Close()

	_, err = io.Copy(dst, src)
	if err != nil {
		return err
	}

	return nil
}
